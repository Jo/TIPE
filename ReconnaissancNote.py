
# importation des librairies
from numpy import linspace, mean 
import numpy as np
from scipy.fftpack import fft, fftfreq
import matplotlib.pyplot as plt
from scipy.io.wavfile import read
import os.path

#Trouve la fréquence du premier La inférieure ou égal à la note. 
def trouverMin(note):
	La = [55, 110, 220, 440, 880, 1760, 3520, 7040, 14080]
	min = La[0]
	i= 0
	while (i < len(La)) & (La[i] <= note):
		min = La[i]
		i = i+1
	return(min,i)

#Donne un tableau de toutes les notes comprises entre la fréquence d'un La (min) et la fréquence du La suivant. (octave à partir du La)
def tabNotes(min):
	notes = []
	if min >= 55 :
		for i in range(0, 13):
			x = min*(2**(i/12))
			notes.append(round(x,2))
	else :
		notes=[32.70]
		for i in range (1,10) :
			x=notes[0]*(2**(i/12))
			notes.append(round(x,2))
	return(notes)



#Trouve le nom de la note correspondant à la fréquence, à partir de la liste contenant cette note. 
def trouverNote(liste, note ,posMin):
	notes = ["La", "La#", "Si", "Do", "Do#", "Ré", "Ré#", "Mi", "Fa", "Fa#", "Sol", "Sol#","La"]
	min = abs(liste[0]-note)
	k = 0
	for i in range(1, len(notes)):
		diff = abs(liste[i]-note)
		if min >= diff:
			min = diff
			k = i
	if k<= 2 :
		posMin+=-1
	return(notes[k],posMin)

#Gère les fréquences en dessous de 55.
def trouverNoteBasseFrq(liste,note):
	notes = ["Do", "Do#", "Ré", "Ré#", "Mi", "Fa", "Fa#", "Sol", "Sol#","La"]
	min = abs(liste[0]-note)
	k = 0
	for i in range(1, len(notes)):
		diff = abs(liste[i]-note)
		if min >= diff:
			min = diff
			k = i
	return(notes[k],0)


def freqToNote(frq):
	x=trouverMin(frq)
	if frq >= 55 :
		#print(tabNotes(x[0]))
		res=trouverNote(tabNotes(x[0]), frq,x[1])
	else :
		res=trouverNoteBasseFrq(tabNotes(frq),frq)
	return(res)





###############################################
# Permet de transformer un son stéréo en mono #
###############################################
def transfomono(son):
	a = np.zeros(len(son))
	for i in range (0,len(son)):
		a[i]=son[i][0]
	return a


########################################
# Permet de trouver le max d'une liste #
########################################
def rangmax(l):
	acc=0
	maxi=0
	rang=0
	for i in range(0,len(l)):
		acc+=1
		if l[i]>maxi:
			maxi=l[i]
			rang=acc
	return rang

##########################################################################
# lecture du fichier son, et définition de la fréquence d'échantillonage #
##########################################################################
def soundToNote(path):
	rate,signal = read(path)
	
	# Permet de transformer le signal stéréo en mono
	signal = transfomono(signal)
	
	# definition du temps, de la durée
	dt = 1./rate 
	FFT_size = 2**25
	NbEch = signal.shape[0]
	
	t = linspace(0,(NbEch-1)*dt,NbEch)
	t = t[0:FFT_size]
	
	######################################
	# calcul de la TFD par l'algo de FFT #
	######################################
	signal = signal[0:FFT_size]
	signal = signal - mean(signal) # soustraction de la valeur moyenne du signal
	signal_FFT = abs(fft(signal))  # on ne recupere que les composantes reelles
	
	# recuperation du domaine frequentiel
	signal_freq = fftfreq(signal.size, dt)
	
	# extraction des valeurs reelles de la FFT et du domaine frequentiel
	signal_FFT = signal_FFT[0:len(signal_FFT)//2]
	signal_freq = signal_freq[0:len(signal_freq)//2]
	
	
	#affichage du signal
	plt.subplot(211)
	plt.title('Signal reel et son spectre')
	plt.plot(t,signal)
	plt.xlabel('Temps (s)'); plt.ylabel('Amplitude')
	
	#affichage du spectre du signal
	plt.subplot(212)
	plt.plot(signal_freq, signal_FFT)
	plt.xlabel('Frequence (Hz)'); plt.ylabel('Amplitude')
	
	a = rangmax(signal_FFT)
	
	
	fondamental = signal_freq[a-1]
	note = freqToNote(fondamental)
	
	
	#plt.show()
	return(fondamental, note)


#soundToNote('800khz.wav')

def main():
	
	while True:
		print("Saisir le chemin d'un fichier audio (.wav)")
		path = input()
		if path == 'q':
			break
		if os.path.isfile(path):
			fondamental, note = soundToNote(path)
			#print("\nQue voulez-vous faire?")
			print("1) Connaître la note \n2) Connaître la fréquence \n3) Afficher les graphes fréquences \nq) Quitter")

			while True:
				print("Que voulez-vous faire?")
				choice = input()
				if  choice== 'q':
					break
				else:
					if choice == '1':
						print("La note enregistrée est :", note)
					else:
						if choice =='2':
							print("La fréquence enregistrée est : ", fondamental)
						else:
							if choice == '3':
								plt.show()



		else:
			print("Le fichier n'existe pas.")


main()